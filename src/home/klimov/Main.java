package home.klimov;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws AWTException, InterruptedException {
        MyRobot myRobot = new MyRobot();
        int mask = InputEvent.BUTTON1_DOWN_MASK;
        TimeUnit.SECONDS.sleep(10);
        while (true) {
            myRobot.botMouseMoveAndClick(539, 553, mask);   //answer
            myRobot.botMouseMoveAndClick(515, 447, mask);   //answer
            myRobot.botMouseMoveAndClick(527, 594, mask);   //answer
            myRobot.botMouseMoveAndClick(373, 567, mask);   //answer
            myRobot.botMouseMoveAndClick(303, 577, mask);   //answer
            myRobot.botMouseMoveAndClick(656, 622, mask);   //answer
            myRobot.botMouseMoveAndClick(433, 566, mask);   //answer
            myRobot.botMouseMoveAndClick(478, 629, mask);   //answer
            myRobot.botMouseMoveAndClick(391, 609, mask);   //answer
            myRobot.botMouseMoveAndClick(659, 519, mask);   //answer
            TimeUnit.SECONDS.sleep(1);
            myRobot.botInputAltPrtSc();
            myRobot.botInputAltTab();                             //move to word
            TimeUnit.SECONDS.sleep(1);
            myRobot.botInputEnter();                              //click Enter
            myRobot.botInputCtrlV();                              //paste
            TimeUnit.SECONDS.sleep(1);
            myRobot.botInputAltTab();                             //mov to test
            TimeUnit.SECONDS.sleep(1);
            myRobot.botMouseMoveAndClick(1341, 861, mask);  //next
            TimeUnit.SECONDS.sleep(1);
        }
    }
}
