package home.klimov;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class MyRobot {
    private Robot bot = new Robot();

    public MyRobot() throws AWTException {
    }

    public void botMouseMoveAndClick(int x, int y, int mask) {  //координаты в paint x:+5; y:+144
        bot.mouseMove(x, y);
        bot.mousePress(mask);
        bot.mouseRelease(mask);
    }

    public void botInputAltPrtSc() {
        bot.keyPress(KeyEvent.VK_ALT);
        bot.keyPress(KeyEvent.VK_PRINTSCREEN);
        bot.keyRelease(KeyEvent.VK_ALT);
        bot.keyRelease(KeyEvent.VK_PRINTSCREEN);
    }

    public void botInputAltTab() {
        bot.keyPress(KeyEvent.VK_ALT);
        bot.keyPress(KeyEvent.VK_TAB);
        bot.keyRelease(KeyEvent.VK_ALT);
        bot.keyRelease(KeyEvent.VK_TAB);
    }

    public void botInputCtrlC() {
        bot.keyPress(KeyEvent.VK_CONTROL);
        bot.keyPress(KeyEvent.VK_C);
        bot.keyRelease(KeyEvent.VK_CONTROL);
        bot.keyRelease(KeyEvent.VK_C);
    }

    public void botInputCtrlV() {
        bot.keyPress(KeyEvent.VK_CONTROL);
        bot.keyPress(KeyEvent.VK_V);
        bot.keyRelease(KeyEvent.VK_CONTROL);
        bot.keyRelease(KeyEvent.VK_V);
    }

    public void botInputEnter() {
        bot.keyPress(KeyEvent.VK_ENTER);
        bot.keyPress(KeyEvent.VK_ENTER);
    }

}
